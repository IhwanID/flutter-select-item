import 'package:flutter/material.dart';

class Subject {
  final int id;
  final String title;

  Subject(this.id, this.title);
}
