import 'package:flutter/material.dart';
import 'package:selectitemlistview/paint.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  double padValue = 0;
  String selected;

  List<Subject> data = <Subject>[
    Subject(1, "IPA"),
    Subject(2, "IPS"),
    Subject(3, "MTK"),
    Subject(3, "PKN"),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: ListView(
        children: List.generate(data.length, (index) {
          return Container(
            margin: EdgeInsets.symmetric(horizontal: 20),
            child: MaterialButton(
              minWidth: 120,
              color: data[index].title == selected ? Colors.blue : Colors.white,
              onPressed: () {
                setState(() {
                  selected = data[index].title;
                  print("${data[index].title.toString()}");
                });
              },
              child: Text(data[index].title),
            ),
          );
        }),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
